package com.sda.ee.mvc.dao;

import com.sda.ee.mvc.model.Auction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AuctionDao implements IAuctionDao {

    private Map<Integer, Auction> auctions = new HashMap<>();

    @Override
    public void addAuction(Auction toAdd) {
        auctions.put(toAdd.getId(), toAdd);
    }

    @Override
    public List<Auction> getAllAuctions() {
        return new ArrayList<>(auctions.values());
    }

}
