package com.sda.ee.mvc.dao;

import com.sda.ee.mvc.model.Product;

import java.util.List;
import java.util.Optional;

public interface IProductDao {
    Optional<Product> getProductWithId(int productId);

    void addProduct(Product toAdd);

    List<Product> getAllProducts();
}
