package com.sda.ee.mvc.services;

import com.sda.ee.mvc.model.Auction;
import com.sda.ee.mvc.model.Product;

import java.util.List;

public interface IAuctionService {
    boolean addAuction(int userId, String title, double price, int amount, Product product);

    List<Auction> getAllAuctions();
}
