package com.sda.ee.mvc.services;

import com.sda.ee.mvc.exceptions.ItemNotFoundException;
import com.sda.ee.mvc.model.Product;
import com.sda.ee.mvc.model.State;

import java.util.List;

public interface IProductService {

    Product getProductWithId(int productId) throws ItemNotFoundException;

    boolean addProduct(String name, State state);

    List<Product> getAllProducts();

}