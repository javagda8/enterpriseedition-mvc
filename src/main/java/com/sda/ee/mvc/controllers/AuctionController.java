package com.sda.ee.mvc.controllers;

import com.sda.ee.mvc.BeanController;
import com.sda.ee.mvc.exceptions.ItemNotFoundException;
import com.sda.ee.mvc.model.Auction;
import com.sda.ee.mvc.model.Product;
import com.sda.ee.mvc.services.IAuctionService;
import com.sda.ee.mvc.services.IProductService;

import java.util.List;

public class AuctionController {

    private IProductService productService = BeanController.getProductService();
    private IAuctionService auctionService = BeanController.getAuctionService();

    public void addAuction(int userId, String title,
                           double price, int amount, int productId) {
        Product product = null;
        try {
            product = productService.getProductWithId(productId);
        } catch (ItemNotFoundException e) {
            System.out.println("This product does not exist: " + e.getMessage());
            return;
        }
        if (product != null) {
            auctionService.addAuction(userId, title, price, amount, product);
        } else {
            System.out.println("This product does not exist");
        }
    }

    public void listAllAuctions() {
        List<Auction> auctionList = auctionService.getAllAuctions();
        auctionList.forEach(System.out::println);
    }
}
