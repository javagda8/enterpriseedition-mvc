package com.sda.ee.mvc.model;

public class Auction {
    private int id;
    private Product product;
    private int ownerId;

    private String title;
    private double price;
    private int amount;

    public Auction(int id, String title, double price, Product product, int amount, int ownerId) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.product = product;
        this.amount = amount;
        this.ownerId = ownerId;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Auction{" +
                "id=" + id +
                ", product=" + product +
                ", ownerId=" + ownerId +
                ", title='" + title + '\'' +
                ", price=" + price +
                ", amount=" + amount +
                '}';
    }
}
