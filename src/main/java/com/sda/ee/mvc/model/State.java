package com.sda.ee.mvc.model;

public enum State {
    USED, NEW, UNKNOWN;
}
