package com.sda.ee.mvc.model;

public class Transaction {
    private int id;
    private int from_user;
    private int to_user;
    private double amount;

    public Transaction(int id, int from_user, int to_user, double amount) {
        this.id = id;
        this.from_user = from_user;
        this.to_user = to_user;
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFrom_user() {
        return from_user;
    }

    public void setFrom_user(int from_user) {
        this.from_user = from_user;
    }

    public int getTo_user() {
        return to_user;
    }

    public void setTo_user(int to_user) {
        this.to_user = to_user;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
