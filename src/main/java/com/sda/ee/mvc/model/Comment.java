package com.sda.ee.mvc.model;

public class Comment {
    private int id;
    private String text;
    private int auctionId;
    private int commenterId;

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", auctionId=" + auctionId +
                ", commenterId=" + commenterId +
                '}';
    }
}
