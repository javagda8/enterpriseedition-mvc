package com.sda;

import java.util.HashMap;
import java.util.Map;

public class Cookies {
    private Map<String, Object> map = new HashMap<>();

    public Object getProperty(String what){
        return map.get(what);
    }

    public void setProperty(String what, Object toWhat){
        map.put(what, toWhat);
    }
}
